//
//  Extension.swift
//  TruthOrDare
//
//  Created by GOD on 03/10/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import Foundation
import UIKit

//MARK:- set padding to text Field
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension String {
    func containsEmoji() -> Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x3030, 0x00AE, 0x00A9,// Special Characters
            0x1D000...0x1F77F,          // Emoticons
            0x2100...0x27BF,            // Misc symbols and Dingbats
            0xFE00...0xFE0F,            // Variation Selectors
            0x1F900...0x1F9FF:          // Supplemental Symbols and Pictographs
                return true
            default:
                continue
            }
        }
        return false
    }
}

extension UIFont {
    
    //NOTICIA TEXT BOLD ITALIC
    class func appFont_Bold_Italic_WithSize(_ fontSize : CGFloat) -> UIFont {
        return UIFont(name: "NoticiaText-BoldItalic", size: fontSize.proportionalFontSize())!
    }
    
    //NOTICIA TEXT ITALIC
    class func appFont_Medium_WithSize(_ fontSize : CGFloat) -> UIFont {
        return UIFont(name: "NoticiaText-Italic", size: fontSize.proportionalFontSize())!
    }
    
    //NOTICIA TEXT REGULAR
    class func appFont_Regular_WithSize(_ fontSize : CGFloat) -> UIFont {
        return UIFont(name: "NoticiaText-Regular", size: fontSize.proportionalFontSize())!
    }
    
    //NOTICIA TEXT BOLD
    class func appFont_Bold_WithSize(_ fontSize : CGFloat) -> UIFont {
        return UIFont(name: "NoticiaText-Bold", size: fontSize.proportionalFontSize())!
    }
    
}

extension CGFloat
{
    func proportionalFontSize() -> CGFloat {
        var sizeToCheckAgainst = self
        if(IS_IPAD_DEVICE())	{
            sizeToCheckAgainst += 8
        }
        else {
            if(IS_IPHONE_6P_OR_6SP()) {
                sizeToCheckAgainst += 1
            }
            else if(IS_IPHONE_6_OR_6S()) {
                sizeToCheckAgainst += 1
            }
            else if(IS_IPHONE_5_OR_5S()) {
                sizeToCheckAgainst -= 0
            }
            else if(IS_IPHONE_4_OR_4S()) {
                sizeToCheckAgainst -= 3
            }
        }
        return sizeToCheckAgainst
    }
}
