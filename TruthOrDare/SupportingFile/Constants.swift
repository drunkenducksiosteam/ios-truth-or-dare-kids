//
//  Constants.swift
//  TruthOrDare
//
//  Created by GOD on 29/09/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import AVFoundation
import Foundation
import UIKit

let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
let mainStoryBoardName = "Main"

let radius : CGFloat = 10.0
let border : CGFloat = 3.0

let modelName = "Truth_or_Dare"
let entityName = "TruthOrDare"
let attributeTask = "task"
let attributeCategory = "category"
let attributeType = "type"
let modeTruth = "Truth"
let modeDare = "Dare"
let playersKey = "players"
let playersScoreKey = "playersScore"
let tagKey = "tag"
let Default = UserDefaults.standard
let categoryArray = ["KidsTruth","KidsDare"]
let bottleArray = ["bottle2.png","bottle3.png","bottle4.png","bottle5.png","bottle6.png","bottle1.png"]
let playerCell = "PlayerTableViewCell"

//MARK:- Controller Identifire
let homeViewController = "HomeViewController"
let mainViewController = "MainViewController"
let addPlayerViewController = "AddPlayerViewController"
let addTruthsViewController = "AddTruthsViewController"
let addDaresViewController = "AddDaresViewController"
let settingViewController = "SettingViewController"
let rootViewController = "rootViewController"

let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String

//Settin ARRAY
let settingKey = ["Game Version", "Game Website Link", "Help & Support", "Developers Link"]
let settingValue = [versionNumber, "www.truthordare.drunkenducks.com", "info@drunkenducks.com", "www.drunkenducks.com"]
let httpURL      = ["http://truthordare.drunkenducks.com/","http://drunkenducks.com/"]
//MUSIC
var IS_SOUND_ON : Bool = true
var musicPlayer : AVAudioPlayer?

//MARK:- Json file details
let fileName = "TaskList"
let fileType = "json"

//CUSTOM TRUTH
let customTruthKey = "customTruths"

//CUSTOM Dare
let customDareKey = "customDares"

//MARK:- Popup Keys
let popUpStoryBoardName = "PopUp"
let selectModePopupView = "SelectModePopupView"
let selectTaskPopupView = "SelectTaskPopupView"
let scoreCardPopupView = "ScoreCardPopupView"
let moveToRootPopupView = "MoveToRootPopupView"

//MARK:- Music File names

let bottleSound = "bottlesound"
let buttonSound = "buttonsound"
let completeSound = "completed"
let forfeitSound = "forfeit"

//MARK:- Google AdMob I\D
let adID = "ca-app-pub-7236719109379341~4880462953"
let bannerID = "ca-app-pub-7236719109379341/4865350197"
let interstitialID = "ca-app-pub-7236719109379341/7645215333"
let adShowCounter:Int = 3

//MARK:- Game Link
let gameLink = "https://itunes.apple.com/app/truth-or-dare-kids-game/id1296581948?ls=1&mt=8"
