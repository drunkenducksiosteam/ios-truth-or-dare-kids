//
//  Functions.swift
//  TruthOrDare
//
//  Created by GOD on 26/09/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

enum GradientDirection {
    case leftToRight
    case rightToLeft
    case topToBottom
    case bottomToTop
}
//MARK:- set gradient background to UIView
func setGradientBackground(view: UIView) {
    
    //YELLOW
    let colorTop = UIColor(red: 253.0/255.0, green: 200.0/255.0, blue: 53.0/255.0, alpha: 1.0).cgColor
    let colorBottom = UIColor(red: 251.0/255.0, green: 192.0/255.0, blue: 45.0/255.0, alpha: 1.0).cgColor
    
    //TEMP
//    let colorTop =  UIColor(red: 246.0/255.0, green: 211.0/255.0, blue: 101.0/255.0, alpha: 1.0).cgColor
//    let colorBottom = UIColor(red: 253.0/255.0, green: 160.0/255.0, blue: 133.0/255.0, alpha: 1.0).cgColor    
    
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [ colorTop, colorBottom]
    gradientLayer.locations = [ 0.0, 1.0]
    gradientLayer.frame = view.bounds
    view.layer.addSublayer(gradientLayer)
}

class Functions {

    //MARK:- PLAY SOUND
    func playSound(fileName : String) {
        let url = Bundle.main.url(forResource: fileName, withExtension: "mp3")!
        
        do {
            musicPlayer = try AVAudioPlayer(contentsOf: url)
            guard musicPlayer != nil else { return }
            
            musicPlayer?.prepareToPlay()
            musicPlayer?.play()
        } catch let error as NSError {
            print(error.description)
        }
    }
}
//MARK:- DEVICE CHECK
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let SCREEN_WIDTH = UIScreen.main.bounds.size.width

//iPhone 4 OR 4S
func IS_IPHONE_4_OR_4S()->Bool{
    let SCREEN_HEIGHT_TO_CHECK_AGAINST:CGFloat = 480
    var device:Bool = false
    
    if(SCREEN_HEIGHT_TO_CHECK_AGAINST == SCREEN_HEIGHT)	{
        device = true
    }
    return device
}

//iPhone 5 OR OR 5C OR 4S
func IS_IPHONE_5_OR_5S()->Bool{
    let SCREEN_HEIGHT_TO_CHECK_AGAINST:CGFloat = 568
    var device:Bool = false
    if(SCREEN_HEIGHT_TO_CHECK_AGAINST == SCREEN_HEIGHT)	{
        device = true
    }
    return device
}

//iPhone 6 OR 6S
func IS_IPHONE_6_OR_6S()->Bool{
    let SCREEN_HEIGHT_TO_CHECK_AGAINST:CGFloat = 667
    var device:Bool = false
    
    if(SCREEN_HEIGHT_TO_CHECK_AGAINST == SCREEN_HEIGHT)	{
        device = true
    }
    return device
}

//iPhone 6Plus OR 6SPlus
func IS_IPHONE_6P_OR_6SP()->Bool{
    let SCREEN_HEIGHT_TO_CHECK_AGAINST:CGFloat = 736
    var device:Bool = false
    
    if(SCREEN_HEIGHT_TO_CHECK_AGAINST == SCREEN_HEIGHT)	{
        device = true
    }
    return device
}

//Check IsiPhone Device
func IS_IPHONE_DEVICE()->Bool{
    let deviceType = UIDevice.current.userInterfaceIdiom == .phone
    return deviceType
}

//Check IsiPad Device
func IS_IPAD_DEVICE()->Bool{
    let deviceType = UIDevice.current.userInterfaceIdiom == .pad
    return deviceType
}
