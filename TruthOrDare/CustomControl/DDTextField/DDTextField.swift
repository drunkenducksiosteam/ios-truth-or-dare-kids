//
//  DDTextField.swift
//  TruthOrDare
//
//  Created by GOD on 30/09/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit


//MARK: ADD TURTHS, ADD DARES, ADD PLAYER
class DDCustomTextField: UITextField {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setLeftPaddingPoints(10.0)
        self.setRightPaddingPoints(10.0)
        self.layer.cornerRadius = radius
        self.layer.borderWidth = border
        self.tintColor = UIColor.black
        self.font = UIFont.appFont_Regular_WithSize(17)
    }
}
