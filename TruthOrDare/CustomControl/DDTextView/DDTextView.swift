//
//  DDTextView.swift
//  TruthOrDare
//
//  Created by GOD on 30/09/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit

//MARK: TextView
class DDCustomTextView: UITextView {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.appFont_Regular_WithSize(17)
    }
}
