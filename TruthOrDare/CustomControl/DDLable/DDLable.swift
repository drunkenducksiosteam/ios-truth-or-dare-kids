//
//  DDLable.swift
//  TruthOrDare
//
//  Created by GOD on 30/09/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit

//MARK: ADD TURTHS, ADD DARES
class DDCustomAddLable: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.appFont_Regular_WithSize(17)
    }
}

//MARK: ADD PLAYES
class DDCustomAddPlayer: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.appFont_Bold_WithSize(17)
    }
}

//MARK: ALL POPUP DISPLAY NAME
class DDNameLable : UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textColor = UIColor.white
        self.font = UIFont.appFont_Bold_WithSize(15)
    }
}

//MARK: HOME BUTTON POPUP , SCORE CARD TITLE
class DDAlertTitle : UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textColor = UIColor.black
        self.font = UIFont.appFont_Regular_WithSize(17)
    }
}
