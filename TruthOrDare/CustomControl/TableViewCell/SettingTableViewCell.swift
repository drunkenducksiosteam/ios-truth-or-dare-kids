//
//  SettingTableViewCell.swift
//  TruthOrDare
//
//  Created by GOD on 27/09/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {

    @IBOutlet var lableKey: UILabel!
    @IBOutlet var lableValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func layoutSubviews() {
        self.lableValue.isUserInteractionEnabled = true
    }

}
