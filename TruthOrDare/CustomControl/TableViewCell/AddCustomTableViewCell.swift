//
//  PlayerTableViewCell.swift
//  Truth or Dare
//
//  Created by GOD on 29/08/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit

class AddCustomTableViewCell: UITableViewCell {

    @IBOutlet var lableName: DDCustomAddLable!
    @IBOutlet var buttonRemove: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.buttonRemove.layer.cornerRadius = self.buttonRemove.frame.height / 2
        self.buttonRemove.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
