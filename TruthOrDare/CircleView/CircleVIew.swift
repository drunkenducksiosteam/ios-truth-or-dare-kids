//
//  CircleVIew.swift
//  SpinTheBottle
//
//  Created by Admin on 12/08/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit



@IBDesignable
class CircleVIew: UIView {
    let chartColors = ["#1A237E",   // #1
        "#00C853",   // #2
        "#FFD600",   // #3
        "#E91E63",   // #4
        "#2196F3",   // #5
        "#FF5722",   // #6
        "#607D8B",   // #7
        "#009688",   // #8
        "#E53935",   // #9
        "#2E7D32",   // #10
        "#795548",   // #11
        "#673AB7",   // #12
        "#006064",   // #13
        "#0D47A1",   // #14
        "#DD2C00",   // #15
        "#4A148C",   // #16
        "#222222",   // #17
        "#0D47A1",   // #18
        "#FF6F00",   // #19
        "#9E9E9E"];  // #20
    let players:[String] = Default.array(forKey: playersKey) as! [String]
    let π:CGFloat = CGFloat(Double.pi)
    
    //MARK: Draw Circle and Call the Draw player name inside it
    override func draw(_ rect:CGRect) {
        let context = UIGraphicsGetCurrentContext()
        let diameter = min(bounds.width, bounds.height)
        let margin:CGFloat = 5
        let circle = UIBezierPath(ovalIn:
            CGRect(x:0, y:0,
                   width:diameter,
                   height:diameter
                ).insetBy(dx: margin, dy: margin))
        let transform = CGAffineTransform(translationX: bounds.width/2 - diameter/2 ,y: 0)
        
        circle.apply(transform)
        let percent = CGFloat(1.0 / CGFloat(players.count))
        let angle = percent * 2.0 * π
        for (index, _) in players.enumerated() {
            let slice = UIBezierPath()
            let radius = diameter / 2 - margin
            let centerPoint = center
            let start =  (angle) - CGFloat(Double.pi / 2.0)
            let end = start  + angle
            slice.move(to: centerPoint)
            slice.addArc(withCenter: centerPoint, radius:radius, startAngle: start, endAngle: end, clockwise: true)
            slice.close()
            let color = UIColor().HexToColor(hexString: self.chartColors[index])
            color.setFill()
            slice.fill()
            slice.lineWidth = 2.0
            slice.stroke(with: .darken, alpha: 1)
            //MARK: Call method to set player name inside circle
            drawText(players[index], centerPoint: centerPoint, radius:radius, angle: start + end)
            context?.translateBy(x: centerPoint.x, y: centerPoint.y)
            context?.rotate(by: angle)
            context?.translateBy(x: -centerPoint.x, y: -centerPoint.y)
        }
        circle.stroke()
     }
    //MARK: Draw player Name inside circle
    func drawText(_ name: String, centerPoint:CGPoint, radius:CGFloat, angle:CGFloat) {
        let context = UIGraphicsGetCurrentContext()
        context?.saveGState()
        context?.translateBy(x: centerPoint.x, y: centerPoint.y)
        context?.rotate(by: angle/2 )
        context?.translateBy(x: -centerPoint.x, y: -centerPoint.y)
        var font = UIFont()
        if IS_IPAD_DEVICE() {
            font = UIFont(name: "HelveticaNeue-Bold", size: 20)!
        } else {
            font = UIFont(name: "HelveticaNeue-Bold", size: 14)!
        }
        let attributes = [NSFontAttributeName: font,
                          NSForegroundColorAttributeName: UIColor.white] as [String : Any]
        
        let transform = context?.ctm
        let radians = atan2((transform?.b)!, (transform?.a)!)
        if abs(radians) > π / 2 && abs(radians) < 3 / 2 * π {
            let textBounds = name.size(attributes: attributes)
            context?.saveGState()
            context?.rotate(by: π)
            name.draw(at: CGPoint(x:-centerPoint.x - radius  + 10, y:-centerPoint.y - textBounds.height/2), withAttributes:attributes)
            context?.restoreGState()
         } else {
            let textBounds = name.size(attributes: attributes)
            name.draw(at: CGPoint(x:centerPoint.x + radius - textBounds.width - 10, y:centerPoint.y - textBounds.height/2), withAttributes:attributes)
        }
        context?.restoreGState()
     }
}
