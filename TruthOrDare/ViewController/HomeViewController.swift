//
//  HomeViewController.swift
//  Truth or Dare
//
//  Created by GOD on 29/08/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet var buttonTopMargine : NSLayoutConstraint!
    
    @IBOutlet var btnSetting : UIButton!
    
    @IBOutlet var btnPlay : UIButton!
    @IBOutlet var btnAddTruths : UIButton!
    @IBOutlet var btnAddDares : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        buttonTopMargine.constant = 130.0
        
        btnSetting.isExclusiveTouch = true
        
        btnPlay.isExclusiveTouch = true
        btnAddTruths.isExclusiveTouch = true
        btnAddDares.isExclusiveTouch = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: playButtopnTapped
    @IBAction func playButtonTapped(_ sender: Any) {
        
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        
        let destVC = self.storyboard?.instantiateViewController(withIdentifier: addPlayerViewController) as! AddPlayerViewController
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    //MARK: buttonAddTruthsClicked
    @IBAction func buttonAddTruths_Clicked(_ sender: Any) {
        
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        
        let objAddTruthVC = self.storyboard?.instantiateViewController(withIdentifier: addTruthsViewController) as! AddTruthsViewController
        self.navigationController?.pushViewController(objAddTruthVC, animated: true)
    }
    
    //MARK: buttonAddDareClicked
    @IBAction func buttonAddDare_Clicked(_ sender: Any) {
        
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        
        let objAddTruthVC = self.storyboard?.instantiateViewController(withIdentifier: addDaresViewController) as! AddDaresViewController
        self.navigationController?.pushViewController(objAddTruthVC, animated: true)
    }
    
    //MARK: SETTING
    @IBAction func buttonSetting_Clicked(_ sender: Any) {
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        
        let objSettingVC = self.storyboard?.instantiateViewController(withIdentifier: settingViewController) as! SettingViewController
        self.navigationController?.pushViewController(objSettingVC, animated: true)
    }
    
}
