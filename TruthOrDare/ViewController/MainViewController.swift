//
//  MainViewController.swift
//  Truth or Dare
//
//  Created by GOD on 29/08/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MainViewController: UIViewController {
    
    //MARK:- Google ad Constant
    var interstitial: GADInterstitial!
    
    //MARK: Spin the bottel
    var locationOfBeganTap: CGPoint!
    var locationOfEndTap: CGPoint!
    var touchDate:Date!
    var touchEndDate:Date!
    var duration:Double  = 0.0
    var degree : CGFloat = 0.0
    
    @IBOutlet var CircleView: CircleVIew!
//    @IBOutlet var bannerView: GADBannerView!
    
    var animator: UIDynamicAnimator?
    @IBOutlet weak var BottleImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    let players:[String] = Default.array(forKey: playersKey) as! [String]
    var timeTimer: Timer?
    
    @IBOutlet weak var btnSound: UIButton!
    @IBOutlet weak var btnShowScoreCard: UIButton!
    @IBOutlet weak var btnChangeBottle: UIButton!
    @IBOutlet var buttonHome : UIButton!
    
    var arr = [String : Any]()
    var currentPlayerName = String()
    var playerIndex = Int()
    var customTask = [String: Any]()
    var adCounter = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupAdmobConfiguration()
        self.defaultConfiguration()
        if Default.array(forKey: customTruthKey) != nil {
            self.customTask[modeTruth] = Default.array(forKey: customTruthKey) as! [String]
        } else {
            self.customTask[modeTruth] = []
        }
        if Default.array(forKey: customDareKey) != nil {
            self.customTask[modeDare] = Default.array(forKey: customDareKey) as! [String]
        } else {
            self.customTask[modeDare] = []
        }
        self.loadData()// Execute these lines after selecting perticular mode i.e 0 for K, 1 for T, 2 for A
        // Do any additional setup after loading the view.
        
        buttonHome.isExclusiveTouch = true
        btnSound.isExclusiveTouch = true
        btnShowScoreCard.isExclusiveTouch = true
        btnChangeBottle.isExclusiveTouch = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if !IS_SOUND_ON {
            self.btnSound.setImage(UIImage(named: "sound-off"), for: .normal)
        }
        else
        {
            self.btnSound.setImage(UIImage(named: "sound-on"), for: .normal)
        }
    }
    
    func loadData(flag : Int = 0) {
      
        //MARK:- Load data from Array
        var tempT = [String]()
        var tempD = [String]()
        
        //Fetch one-by-one records and check either truth or dare
        for key in categoryArray{
            let items = DATA[key]!
            for item in items {
                if key == categoryArray[0]{
                    tempT.append(item)
                } else {
                    tempD.append(item)
                }
            }
        }
        if flag == 1 {
            //updte truth in  main array
            self.arr[modeTruth] = tempT
        }else if flag == 2 {
            //updte dare in  main array
            self.arr[modeDare] = tempD
        } else {
            //updte whole main array
            self.arr[modeTruth] = tempT
            self.arr[modeDare] = tempD
        }
    }
    
    func selectType() {
        //MARK:- Give option to select T or D after bottle is stopped
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .animation(.slideUp),
                    .scrollable(false),
                    .backgroundStyle(.blackFilter(alpha: 0.7))
                ]
            )
            .didShowHandler { popup in
                
            }
            .didCloseHandler { popup in
                if Default.integer(forKey: tagKey) == 0{
                    self.getTask(mode: modeTruth)// If T is selected
                }else {
                    self.getTask(mode: modeDare)// If D is selected
                }
        }
        let container = SelectModePopupView.instance()
        container.setData(data: "\(self.currentPlayerName)'s turn")
        container.closeHandler = { _ in
            popup.dismiss()
        }
        popup.show(container)
    }
    
    //MARK:- Select Random Question for selected mode
    func getTask(mode: String) {
        let tempCustomTask = self.customTask[mode] as! [String]
        //if custom truth or dare array is not empty the select custom array else default array
        var temp = [String]()
        var index = uint()
        var selectedQuestion = String()
        
        if tempCustomTask.count != 0 {
            temp = self.customTask[mode] as! [String]
            index = arc4random_uniform(UInt32(temp.count))
            selectedQuestion = temp[Int(index)]
            temp.remove(at: Int(index)) // remove that selected Q, to avoid Duplication
            self.customTask[mode] = temp
            //print(self.customTask)
        } else {
            //if array is NULL then reload data from Db
            temp = self.arr[mode] as! [String]
            if temp.count <= 1 {
                if mode == modeTruth {
                    self.loadData(flag: 1)
                } else {
                    self.loadData(flag: 2)
                }
            }
            index = arc4random_uniform(UInt32(temp.count))
            selectedQuestion = temp[Int(index)]
            temp = self.arr[mode] as! [String]
            temp.remove(at: Int(index)) // remove that selected Q, to avoid Duplication
            self.arr[mode] = temp
            //print(self.arr)
        }
        self.showDataOnPopUp(data: selectedQuestion)
    }
    
    //MARK: SHOW DATA ON POPUP
    private func showDataOnPopUp(data: String) {
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .animation(.fadeIn),
                    .scrollable(false),
                    .backgroundStyle(.blackFilter(alpha: 0.7))
                ]
            )
            .didShowHandler { popup in
                
            }
            .didCloseHandler { popup in
                if Default.integer(forKey: tagKey) == 1{
                    var scoreList = Default.array(forKey: playersScoreKey) as! [Int]
                    var update = scoreList[self.playerIndex]
                    update = update + 1
                    scoreList[self.playerIndex] = update
                    Default.set(scoreList, forKey: playersScoreKey)
                }
        }
        let container = SelectTaskPopupView.instance()
        container.setData(task: data, currentPlayerName: "\(currentPlayerName)'s turn")
        container.closeHandler = { _ in
            popup.dismiss()
        }
        popup.show(container)
    }
    //MARK:- Sound on/off action
    @IBAction func soundButtonTapped(_ sender: UIButton) {
        if IS_SOUND_ON {
            self.btnSound.setImage(UIImage(named: "sound-off"), for: .normal)
            IS_SOUND_ON = false
        } else {
            self.btnSound.setImage(UIImage(named: "sound-on"), for: .normal)
            IS_SOUND_ON = true
            Functions().playSound(fileName: buttonSound)
        }
    }
    //MARK:- ScoreCard action
    @IBAction func scoreCardButtonTapped(_ sender: UIButton) {
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        self.showScoreCard()
    }
    //MARK:- Change Bottle action
    @IBAction func changeBottleButtonTapped(_ sender: UIButton) {
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        sender.tag = sender.tag + 1
        if sender.tag > bottleArray.count - 1 {
            sender.tag = 0
        }
        self.BottleImageView.image = UIImage(named: bottleArray[sender.tag])
    }
    //MARK:- Home Button action
    @IBAction func homeButtonTapped(_ sender: UIButton) {
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .animation(.slideUp),
                    .scrollable(false),
                    .backgroundStyle(.blackFilter(alpha: 0.7))
                ]
            )
            .didShowHandler { popup in
                
            }
            .didCloseHandler { popup in
                if Default.integer(forKey: tagKey) == 0{
                    APPDELEGATE.navigateToRoot()
                }
        }
        let container = MoveToRootPopupView.instance()
        container.closeHandler = { _ in
            popup.dismiss()
        }
        popup.show(container)
    }
    
    func showScoreCard() {
        //MARK:- Give option to select T or D after bottle is stopped
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .animation(.slideUp),
                    .scrollable(false),
                    .backgroundStyle(.blackFilter(alpha: 0.7))
                ]
            )
            .didShowHandler { popup in
            }
            .didCloseHandler { popup in
            }
        let container = ScoreCardPopupView.instance()
        //container.setData(data: "\(self.currentPlayerName)'s turn")
        container.closeHandler = { _ in
            popup.dismiss()
        }
        popup.show(container)
    }
    
    //MARK:- Unable/Disable userIntrection when bottle is spin
    func disableButton() {
        self.btnSound.isUserInteractionEnabled = false
        self.btnShowScoreCard.isUserInteractionEnabled = false
        self.btnChangeBottle.isUserInteractionEnabled = false
        self.buttonHome.isUserInteractionEnabled = false
    }
    //MARK:- Unable userIntrection when bottle is spin
    func unableButton() {
        self.btnSound.isUserInteractionEnabled = true
        self.btnShowScoreCard.isUserInteractionEnabled = true
        self.btnChangeBottle.isUserInteractionEnabled = true
        self.buttonHome.isUserInteractionEnabled = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension MainViewController{
    //MARK: Set the default configuration for Circle and Bottle rotation
    func defaultConfiguration() {
        containerView.insertSubview(CircleView, at: 0)        
        CircleView.translatesAutoresizingMaskIntoConstraints = false
        CircleView.heightAnchor.constraint(equalTo: containerView.heightAnchor).isActive = true
        CircleView.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        CircleView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        CircleView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        CircleView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        CircleView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        CircleView.layoutIfNeeded()
        self.BottleImageView.isUserInteractionEnabled = true
        self.addRotationGesture()
    }
    //MARK: Add Rotation gesture to bottle
    func addRotationGesture() {
        self.BottleImageView.isUserInteractionEnabled = true
        let rotation = UIPanGestureRecognizer(target: self, action: #selector(self.detectGesture))
        self.BottleImageView.addGestureRecognizer(rotation)
        
    }
    //MARK: Detect the gesture on bottle and call rotate method for rotation
    func detectGesture(sender: UIPanGestureRecognizer) {
        
        if sender.state == .began{
            locationOfBeganTap = sender.location(in: self.view)
            self.touchDate =  NSDate() as Date!
        }else if sender.state == .ended{
            locationOfEndTap = sender.location(in: self.view)
            self.touchEndDate = Date()
            //let string = Date().offsetFrom(date: self.touchDate)
            self.duration = 4.0
            if sender.direction!.rawValue == Direction.Left.rawValue || sender.direction!.rawValue == Direction.Down.rawValue{
                self.degree = 180
                self.rotate()
            }else {
                self.degree = -180
                self.rotate()
            }
        }
    }
    
    //MARK:Spin or Rotate the bottel and call rotateRandom to stop at random point
    func rotate(){
        if IS_SOUND_ON  {
            Functions().playSound(fileName: bottleSound)
        }
        self.adCounter += 1
        
        self.setupAdmobConfiguration()
        self.disableButton()
        UIView.animate(withDuration: TimeInterval(self.duration), delay: 0.0, options: [.curveEaseOut],animations: { () -> Void in
            let randomNumber = self.randomInt(min: 40, max: 50)
            for _ in 0..<randomNumber{
                self.BottleImageView.transform = self.BottleImageView.transform.rotated(by: self.degree * 0.0174532925)
            }
        }, completion:{Void in
            let radians = atan2f(Float(self.BottleImageView.transform.b), Float(self.BottleImageView.transform.a));
            let degrees = radians *  Float(180 / Double.pi);
            if degrees < 0 {
                self.getRercord(degree: CGFloat(360 + degrees))
            }else{
                self.getRercord(degree: CGFloat(degrees))
            }
            if (self.adCounter % adShowCounter) == 0 {
                self.createAndLoadInterstitial()
            }
            self.unableButton()
        })
        rotateRandom()
    }
    //MARK: Stop the bottel at random point
    func rotateRandom(){
        let diceRoll = CGFloat(arc4random_uniform(360))
        let rotate = diceRoll
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut,      animations: { () -> Void in
            self.BottleImageView.transform = self.BottleImageView.transform.rotated(by: rotate)
        }, completion:{Void in
            //print(self.BottleImageView.transform)
        })
    }
    //MARK: Generate the random number by given range
    func randomInt(min: Int, max:Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    //MARK: Get the Player Name when bottle stop
    func getRercord(degree:CGFloat)  {
        //print(degree)
        let part:CGFloat = CGFloat(CGFloat(360) / CGFloat(players.count))
        for i in 0..<players.count{
            let min = CGFloat(part * (CGFloat(i)))
            let max = CGFloat(part * (CGFloat(i) + 1))
            if degree >= min && degree <= max{
                if i == 0 {
                    self.currentPlayerName = players[players.count - 1 ]
                    self.playerIndex = players.count - 1
                }else{
                    self.currentPlayerName = players[Int(i)-1]
                    self.playerIndex = i - 1
                }
                self.selectType()
            }
        }
    }
}
public enum Direction: Int {
    case Up
    case Down
    case Left
    case Right
    
    public var isX: Bool { return self == .Left || self == .Right }
    public var isY: Bool { return !isX }
}

public extension UIPanGestureRecognizer {
    
    public var direction: Direction? {
        let velocity2 = velocity(in: self.view)
        _ = fabs(velocity2.y) > fabs(velocity2.x)
        //print(velocity2)
        switch (velocity2.x, velocity2.y) {
        //case (true, _, let y) where y < 0: return .Up
        case (let x, let y) where y > 0 && x > 0 : return .Left
        case (let x, let y) where y > 0 && x < 0 : return .Right
        case (let x, let y) where y < 0 && x > 0 : return .Right
            //case (let x, let y) where y < 0 && x < 0 : return .Left
            //case (false, let x, _) where x > 0: return .Right
            
        //case (false, let x, _) where x < 0: return .Left
        default: return .Left
        }
    }
}

//MARK:- Google Ad
extension MainViewController:GADInterstitialDelegate {
    func createAndLoadInterstitial() {
        print(interstitial.isReady)
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
           // interstitial.present(fromRootViewController: self)
           // self.setupAdmobConfiguration()
            //self.createAndLoadInterstitial()
            print("Ad wasn't ready")
        }
    }
    func setupAdmobConfiguration() {
        
        interstitial = GADInterstitial(adUnitID: interstitialID)
        interstitial.delegate = self
        let request = GADRequest()
       // request.testDevices = [ kGADSimulatorID, "C07TP5LRG1HW" ]
        interstitial.load(request)
        
    }
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        self.setupAdmobConfiguration()
    }
//    func interstitialDidDismissScreen(ad: GADInterstitial!) {
//        self.setupAdmobConfiguration()
//    }
}
