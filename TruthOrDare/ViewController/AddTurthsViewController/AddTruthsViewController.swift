//
//  AddTruthsViewController.swift
//  TruthOrDare
//
//  Created by GOD on 20/09/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit

class AddTruthsViewController: UIViewController {

    @IBOutlet var tableViewAddTruths : UITableView!
    @IBOutlet weak var textFieldAddTruths : DDCustomTextField!
    @IBOutlet var buttonAddTruth : UIButton!
    
    @IBOutlet var btnBack : UIButton!
    
    var aryTruths = [String]()
    var identifier : String = "addTruthsCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if Default.array(forKey: customTruthKey) != nil {
            self.aryTruths = Default.array(forKey: customTruthKey) as! [String]
        }
        tableViewAddTruths.delegate = self
        tableViewAddTruths.dataSource = self
        
        tableViewAddTruths.tableFooterView = UIView()
        
        if IS_IPAD_DEVICE() {
            tableViewAddTruths.estimatedRowHeight = 100.0
        }
        else
        {
            tableViewAddTruths.estimatedRowHeight = 80.0
        }
        
        buttonAddTruth.layer.cornerRadius = radius
        
        btnBack.isExclusiveTouch = true
        buttonAddTruth.isExclusiveTouch = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: buttonAddTruths_Clicked
    @IBAction func buttonAddTruths_Clicked(_ sender: Any) {
        
        if IS_SOUND_ON && textFieldAddTruths.text != "" {
            Functions().playSound(fileName: buttonSound)
        }
        let customTruth = textFieldAddTruths.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if !customTruth.isEmpty {
            if !customTruth.containsEmoji() {
                if customTruth.characters.count < 102 {
                    aryTruths.append(customTruth)
                    Default.set(aryTruths, forKey: customTruthKey)
                }else {
                    self.view.makeToast("Maximum 100 character required", duration: 2.0, position: .center)
                }
            } else {
                self.view.makeToast("Emoji are not allowed", duration: 2.0, position: .center)
            }
        } else {
            self.view.makeToast("Minimum 1 character required", duration: 2.0, position: .center)
        }
        self.textFieldAddTruths.text = ""
        self.tableViewAddTruths.reloadData()
        //print(Default.array(forKey: customTruthKey)!)
    }
    
    //MARK: removeTruths
    func removeTruths(_ sender: UIButton) {
        
        if IS_SOUND_ON {
            Functions().playSound(fileName: buttonSound)
        }
        
        self.aryTruths.remove(at: sender.tag)
        Default.set(aryTruths, forKey: customTruthKey)
        self.tableViewAddTruths.reloadData()
    }
    
    //MARK: backButtonTapped
    @IBAction func backButtonTapped(_ sender: Any) {
        
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: UITextFieldDelegate
extension AddTruthsViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if(string == "") {
            return true
        }
        
        if (textField.text?.characters.count)! > 100 {
            return false
        }
        return true
    }
}

extension AddTruthsViewController : UITableViewDelegate, UITableViewDataSource  {
    
    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return aryTruths.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let addTruthCell : AddCustomTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! AddCustomTableViewCell
        
        addTruthCell.lableName.text = aryTruths[indexPath.row]
        addTruthCell.buttonRemove.tag = indexPath.row
        addTruthCell.buttonRemove.addTarget(self, action: #selector(removeTruths(_:)), for: .touchUpInside)
        
        return addTruthCell
    }
}
