//
//  AddDaresViewController.swift
//  TruthOrDare
//
//  Created by GOD on 21/09/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit

class AddDaresViewController: UIViewController {

    @IBOutlet var tableViewAddDares : UITableView!
    @IBOutlet weak var textFieldAddDares : DDCustomTextField!
    @IBOutlet var buttonAddDare : UIButton!
    @IBOutlet var btnBack : UIButton!
    
    var aryDares = [String]()
    var identifier : String = "addDaresCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if Default.array(forKey: customDareKey) != nil {
            self.aryDares = Default.array(forKey: customDareKey) as! [String]
        }
        tableViewAddDares.delegate = self
        tableViewAddDares.dataSource = self
        
        tableViewAddDares.tableFooterView = UIView()
        if IS_IPAD_DEVICE() {
            tableViewAddDares.estimatedRowHeight = 100.0
        }
        else
        {
            tableViewAddDares.estimatedRowHeight = 80.0
        }
        
        buttonAddDare.layer.cornerRadius = radius
        
        btnBack.isExclusiveTouch = true
        buttonAddDare.isExclusiveTouch = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: buttonAddTruths_Clicked
    @IBAction func buttonAddDares_Clicked(_ sender: Any) {
        
        if IS_SOUND_ON && textFieldAddDares.text != "" {
            Functions().playSound(fileName: buttonSound)
        }
        let customDare = textFieldAddDares.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if !customDare.isEmpty {
            if !customDare.containsEmoji() {
                if customDare.characters.count < 102 {
                    aryDares.append(customDare)
                    Default.set(aryDares, forKey: customTruthKey)
                }else {
                    self.view.makeToast("Maximum 100 character required", duration: 2.0, position: .center)
                }
            } else {
                self.view.makeToast("Emoji are not allowed", duration: 2.0, position: .center)
            }
        } else {
            self.view.makeToast("Minimum 1 character required", duration: 2.0, position: .center)
        }

        self.textFieldAddDares.text = ""
        self.tableViewAddDares.reloadData()
        //print(Default.array(forKey: customDareKey)!)
    }
    
    //MARK: removeTruths
    func removeDares(_ sender: UIButton) {
        if IS_SOUND_ON {
            Functions().playSound(fileName: buttonSound)
        }
        
        self.aryDares.remove(at: sender.tag)
        Default.set(aryDares, forKey: customDareKey)
        self.tableViewAddDares.reloadData()
    }
    
    //MARK: backButtonTapped
    @IBAction func backButtonTapped(_ sender: Any) {
        
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK: UITextFieldDelegate
extension AddDaresViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if(string == "") {
            return true
        }
        
        if (textField.text?.characters.count)! > 100 {
            return false
        }
        return true
    }
}

extension AddDaresViewController: UITableViewDataSource, UITableViewDelegate {
    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return aryDares.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let addTruthCell : AddCustomTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! AddCustomTableViewCell
        addTruthCell.lableName.text = aryDares[indexPath.row]
        addTruthCell.buttonRemove.tag = indexPath.row
        addTruthCell.buttonRemove.addTarget(self, action: #selector(removeDares(_:)), for: .touchUpInside)
        return addTruthCell
    }
}
