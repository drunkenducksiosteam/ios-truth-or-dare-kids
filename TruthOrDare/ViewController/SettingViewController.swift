//
//  SettingViewController.swift
//  TruthOrDare
//
//  Created by GOD on 27/09/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {
    
    var identifier : String = "settingCell"
    @IBOutlet var soundOnOff: UISwitch!
    @IBOutlet var tblSettings: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblSettings.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !IS_SOUND_ON {
            self.soundOnOff.isOn = false
        }
        else
        {
            self.soundOnOff.isOn = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Back button
    @IBAction func backButtonTapped(_ sender: Any) {
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Switch Value Changed
    @IBAction func valueChanged(_ sender: Any) {
        
        if self.soundOnOff.isOn {
            IS_SOUND_ON = true
        }
        else
        {
            IS_SOUND_ON = false
        }
    }
}

extension SettingViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }
    func handleTap(sender: UITapGestureRecognizer) {
        let UILabelObject = sender.view as! UILabel
        switch UILabelObject.tag {
        case 1:
            UIApplication.shared.openURL(NSURL(string: httpURL[0])! as URL)
        case 3:
            UIApplication.shared.openURL(NSURL(string: httpURL[1])! as URL)
        default:
            break;
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let settingCell : SettingTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! SettingTableViewCell
        settingCell.lableKey.text = settingKey[indexPath.row]
        settingCell.lableValue.text = settingValue[indexPath.row]
        return settingCell
    }
    
}
