//
//  AddPlayerViewController.swift
//  Truth or Dare
//
//  Created by GOD on 29/08/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AddPlayerViewController: UIViewController {

    @IBOutlet var bannerView: GADBannerView!
    @IBOutlet var playerTableView: UITableView!
    @IBOutlet var txtPlayerName: DDCustomTextField!
    @IBOutlet var buttonAddPlayer : UIButton!
    
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var btnPlay : UIButton!
    @IBOutlet var btnAddPlayer : UIButton!
    
    var players = [String]()
    var score = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if Default.array(forKey: playersKey) != nil {
            self.players = Default.array(forKey: playersKey) as! [String]
        }
        self.playerTableView.tableFooterView = UIView()
        buttonAddPlayer.layer.cornerRadius = radius
        
        btnBack.isExclusiveTouch = true
        btnPlay.isExclusiveTouch = true
        btnAddPlayer.isExclusiveTouch = true
        
        bannerView.adUnitID = bannerID
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: AddNewPlayerButtonTapped
    @IBAction func AddNewPlayerButtonTapped(_ sender: Any) {
        
        if IS_SOUND_ON && txtPlayerName.text != "" {
            Functions().playSound(fileName: buttonSound)
        }
        if players.count < 20 {
            let playerName = txtPlayerName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            if !playerName.isEmpty {
                if !playerName.containsEmoji() {
                    if playerName.characters.count < 16 {
                        if let score : [Int] = Default.array(forKey: playersScoreKey) as? [Int] {
                            self.score = score
                        }
                        players.append(playerName)
                        Default.set(players, forKey: playersKey)
                        score.append(0)
                        Default.set(score, forKey: playersScoreKey)
                    }else {
                        self.view.makeToast("Maximum 15 character required", duration: 2.0, position: .center)
                    }
                } else {
                    self.view.makeToast("Emoji are not allowed", duration: 2.0, position: .center)
                }
            } else {
                self.view.makeToast("Minimum 1 character required", duration: 2.0, position: .center)
            }
            self.txtPlayerName.text = ""
            self.playerTableView.reloadData()
        } else {
            self.view.makeToast("Maximum 20 players are allowed", duration: 2.0, position: .center)
        }
        
    }
    
    //MARK: backButtonTapped
    @IBAction func backButtonTapped(_ sender: Any) {
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: playButtonTapped
    @IBAction func playButtonTapped(_ sender: Any) {
        
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        
        if players.count >= 2 {
            let destVC = self.storyboard?.instantiateViewController(withIdentifier: mainViewController) as! MainViewController
            self.navigationController?.pushViewController(destVC, animated: true)
        } else{
            self.view.makeToast("Minimum 2 players required", duration: 2.0, position: .bottom)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: removePlayers
    func removePlayer(_ sender: UIButton) {
        
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        
        self.players.remove(at: sender.tag)
        Default.set(players, forKey: playersKey)
        self.playerTableView.reloadData()
    }

}

//MARK: UITextFieldDelegate
extension AddPlayerViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if(string == "") {
            return true
        }
        
        if (textField.text?.characters.count)! > 14 {
            return false
        }
        return true
    }
}

//MARK:- Tableview Delegate & datasource Methods
extension AddPlayerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if IS_IPAD_DEVICE() {
            return 80.0
        }
        return 60.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: playerCell) as! AddPlayerTableViewCell
        cell.lableName.text = players[indexPath.row]
        cell.buttonRemove.tag = indexPath.row
        cell.buttonRemove.addTarget(self, action: #selector(removePlayer(_:)), for: .touchUpInside)
        return cell
    }
}
