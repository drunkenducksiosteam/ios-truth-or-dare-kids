//
//  PopUpView2.swift
//  Truth or Dare
//
//  Created by GOD on 29/08/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit

class SelectTaskPopupView: UIViewController, PopupContentViewController {
    
    @IBOutlet weak var txtViewTask: DDCustomTextView!
    var closeHandler: (() -> Void)?
    var task = "Data"
    var currentPlayerName = "ABC's turn"
    var height : CGFloat = 0.0
    
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet var btnUIView: UIView!
    
    @IBOutlet var textviewHeight : NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstButton.isExclusiveTouch = true
        secondButton.isExclusiveTouch = true
        
        setGradientBackground(view: self.view)
        self.view.bringSubview(toFront: btnUIView)
        self.view.bringSubview(toFront: firstButton)
        self.view.bringSubview(toFront: secondButton)
        self.view.bringSubview(toFront: playerName)
        self.view.bringSubview(toFront: txtViewTask)
        
        self.view.layer.borderWidth = 5.0
        self.view.layer.borderColor = UIColor.white.cgColor
        self.view.layer.masksToBounds = true
        
        self.txtViewTask.text = self.task
        self.txtViewTask.textContainer.maximumNumberOfLines = 5
        self.playerName.text = currentPlayerName
        
        if IS_IPAD_DEVICE() {
            self.view.frame.size = CGSize(width: self.view.frame.size.width - 300 ,height: self.view.frame.size.height)
        } else if IS_IPHONE_5_OR_5S() || IS_IPHONE_4_OR_4S() {
            self.view.frame.size = CGSize(width: self.view.frame.size.width - 50 ,height: self.view.frame.size.height)
        } else {
            self.view.frame.size = CGSize(width: self.view.frame.size.width - 100 ,height: self.view.frame.size.height)
        }
        
        // Do any additional setup after loading the view.
    }
    func setData(task: String, currentPlayerName: String) {
        self.task = task
        self.currentPlayerName = currentPlayerName
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    class func instance() -> SelectTaskPopupView {
        let storyboard = UIStoryboard(name: popUpStoryBoardName, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: selectTaskPopupView) as! SelectTaskPopupView
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
       
        if IS_IPAD_DEVICE()
        {
            if txtViewTask.text.characters.count >= 60 {
                return CGSize(width: popupController.popupView.frame.size.width,height: 400)
            }
            return CGSize(width: popupController.popupView.frame.size.width,height: 300)
        }
        if txtViewTask.text.characters.count >= 60 {
            return CGSize(width: popupController.popupView.frame.size.width,height: 300)
        }
        return CGSize(width: popupController.popupView.frame.size.width,height: 250)
    }
    
    @IBAction func didTapCloseButton(_ sender: UIButton) {
        
        if IS_SOUND_ON  {
            if sender.tag == 0 {
                Functions().playSound(fileName: forfeitSound)
            }
            else{
                Functions().playSound(fileName: completeSound)
            }
        }
        Default.set(sender.tag, forKey: tagKey)
        closeHandler?()
    }
    
}


