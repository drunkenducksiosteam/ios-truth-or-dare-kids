//
//  PopUpView4.swift
//  TruthOrDare
//
//  Created by GOD on 26/09/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit

class MoveToRootPopupView: UIViewController, PopupContentViewController {
    
    var closeHandler: (() -> Void)?
    
    @IBOutlet var viewClose: UIView!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        yesButton.isExclusiveTouch = true
        noButton.isExclusiveTouch = true
        
        setGradientBackground(view: self.view)
        self.view.bringSubview(toFront: viewClose)
        self.view.bringSubview(toFront: yesButton)
        self.view.bringSubview(toFront: noButton)
        
        
        self.view.layer.borderWidth = 5.0
        self.view.layer.borderColor = UIColor.white.cgColor
        self.view.layer.masksToBounds = true
        if IS_IPAD_DEVICE() {
            self.view.frame.size = CGSize(width: self.view.frame.size.width - 300 ,height: self.view.frame.size.height)
        } else if IS_IPHONE_5_OR_5S() || IS_IPHONE_4_OR_4S() {
            self.view.frame.size = CGSize(width: self.view.frame.size.width - 50 ,height: self.view.frame.size.height)
        } else {
            self.view.frame.size = CGSize(width: self.view.frame.size.width - 100 ,height: self.view.frame.size.height)
        }
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    class func instance() -> MoveToRootPopupView {
        let storyboard = UIStoryboard(name: popUpStoryBoardName, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: moveToRootPopupView) as! MoveToRootPopupView
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        if IS_IPAD_DEVICE() {
            return CGSize(width: popupController.popupView.frame.size.width,height: 200)
        }
        return CGSize(width: popupController.popupView.frame.size.width,height: 140)
    }
    
    @IBAction func didTapCloseButton(_ sender: UIButton) {
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        Default.set(sender.tag, forKey: tagKey)
        closeHandler?()
    }
}

