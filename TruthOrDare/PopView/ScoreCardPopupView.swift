//
//  PopUpView3.swift
//  TruthOrDare
//
//  Created by GOD on 26/09/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit

class ScoreCardPopupView: UIViewController, PopupContentViewController {
    
    var closeHandler: (() -> Void)?
    var task = "Data"
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var scoreTableView: UITableView!
    @IBOutlet var scoreCardLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setGradientBackground(view: self.view)
        self.view.bringSubview(toFront: closeButton)
        self.view.bringSubview(toFront: scoreTableView)
        self.view.bringSubview(toFront: scoreCardLabel)
        
        self.view.layer.borderWidth = 5.0
        self.view.layer.borderColor = UIColor.white.cgColor
        self.view.layer.masksToBounds = true
        if IS_IPAD_DEVICE() {
            self.view.frame.size = CGSize(width: self.view.frame.size.width - 300 ,height: self.view.frame.size.height)
        } else if IS_IPHONE_5_OR_5S() || IS_IPHONE_4_OR_4S() {
            self.view.frame.size = CGSize(width: self.view.frame.size.width - 50 ,height: self.view.frame.size.height)
        } else {
            self.view.frame.size = CGSize(width: self.view.frame.size.width - 100 ,height: self.view.frame.size.height)
        }
        self.scoreTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    func setData(task: String, currentPlayerName: String) {
        self.task = task
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    class func instance() -> ScoreCardPopupView {
        let storyboard = UIStoryboard(name: popUpStoryBoardName, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: scoreCardPopupView) as! ScoreCardPopupView
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        if IS_IPAD_DEVICE()
        {
            return CGSize(width: popupController.popupView.frame.size.width,height: 450)
        }
        return CGSize(width: popupController.popupView.frame.size.width,height: 300)
    }
    
    @IBAction func didTapCloseButton(_ sender: UIButton) {
        if IS_SOUND_ON  {
            Functions().playSound(fileName: "buttonsound")
        }
        closeHandler?()
    }
}

extension ScoreCardPopupView : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if IS_IPAD_DEVICE() {
            return 80.0
        }
        return 60.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Default.array(forKey: playersKey)?.count)!
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let scoreCell = tableView.dequeueReusableCell(withIdentifier: "scoreCell", for: indexPath)
        scoreCell.selectionStyle = .none
        let playerList = Default.array(forKey: playersKey) as! [String]
        let playerScore = Default.array(forKey: playersScoreKey) as! [Int]
        scoreCell.textLabel?.text = playerList[indexPath.row]
        scoreCell.detailTextLabel?.text = String(describing: playerScore[indexPath.row])
        return scoreCell
    }
}
