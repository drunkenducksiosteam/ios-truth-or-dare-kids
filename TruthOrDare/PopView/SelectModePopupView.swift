//
//  PopUpView1.swift
//  Truth or Dare
//
//  Created by GOD on 29/08/17.
//  Copyright © 2017 GOD. All rights reserved.
//

import UIKit

class SelectModePopupView: UIViewController, PopupContentViewController {
    
    @IBOutlet weak var lblStringData: DDNameLable!
    
    var closeHandler: (() -> Void)?
    var data = "Data"
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstButton.isExclusiveTouch = true
        secondButton.isExclusiveTouch = true
        
        setGradientBackground(view: self.view)
        self.view.bringSubview(toFront: firstButton)
        self.view.bringSubview(toFront: secondButton)
        self.view.bringSubview(toFront: lblStringData)
        
        // Do any additional setup after loading the view.
        self.view.layer.borderWidth = 5.0
        self.view.layer.borderColor = UIColor.white.cgColor
        self.view.layer.masksToBounds = true
        self.lblStringData.text = self.data
        if IS_IPAD_DEVICE() {
            self.view.frame.size = CGSize(width: self.view.frame.size.width - 300 ,height: self.view.frame.size.height)
        } else if IS_IPHONE_5_OR_5S() || IS_IPHONE_4_OR_4S() {
            self.view.frame.size = CGSize(width: self.view.frame.size.width - 50 ,height: self.view.frame.size.height)
        } else {
            self.view.frame.size = CGSize(width: self.view.frame.size.width - 100 ,height: self.view.frame.size.height)
        }
    }
    
    func setData(data: String) {
        self.data = data
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class func instance() -> SelectModePopupView {
        let storyboard = UIStoryboard(name: popUpStoryBoardName, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: selectModePopupView) as! SelectModePopupView
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: popupController.popupView.frame.size.width,height: 300)
    }
    
    @IBAction func didTapCloseButton(_ sender: UIButton) {
        
        if IS_SOUND_ON  {
            Functions().playSound(fileName: buttonSound)
        }
        Default.set(sender.tag, forKey: tagKey)
        closeHandler?()
    }
}
